--[[
	Filesystem
	explorer
	by Creator
]]--
local args = {...}
local ignore = {}
if args[3] then
	local file = fs.open(args[3],"r")
	local data = file.readAll()
	file.close()
	for token in string.gmatch(data,"[^\n]+") do
		ignore[#ignore+1] = token
	end
end

local filesystem = {}

local function readFile(path)
	local file = fs.open(path,"r")
	local variable = file.readAll()
	file.close()
	return variable
end

local function isNotBanned(path)
	for i,v in pairs(ignore) do
		if v == path then
			return false
		end
	end
	return true
end

local function explore(dir)
	local buffer = {}
    local fileCount = 0
	local sBuffer = fs.list(dir)
	for i,v in pairs(sBuffer) do
		sleep(0.05)
		if isNotBanned(dir.."/"..v) then
			if fs.isDir(dir.."/"..v) then
				if v ~= ".git" then
					print("Compressing directory: "..dir.."/"..v)
					buffer[v] = explore(dir.."/"..v)
				end
			else
                fileCount = fileCount + 1
				print("Compressing file: "..dir.."/"..v)
				buffer[v] = readFile(dir.."/"..v)
			end
		end
	end
	return buffer, fileCount
end

append = [[
function progBar(y,a,b,newLine)
  local perc = math.floor((a / b) * 100) .. "%"
  local ox, oy, ot, ob = term.getCursorPos(), term.getTextColor(), term.getBackgroundColor()
  local w, h = term.getSize()
  local percNum = math.floor((a / b) * ((w * 1) - 7))
  term.setCursorPos(1, y)
  term.setBackgroundColor(colors.black)
  term.setTextColor(term.isColor() and colors.yellow or colors.white)
  term.write("[")
  term.setCursorPos((w * 1) - 5, y)
  term.setBackgroundColor(colors.black)
  term.setTextColor(term.isColor() and colors.yellow or colors.white)
  term.write("]")
  term.setCursorPos(2, y)
  term.setBackgroundColor(colors.white)
  term.setTextColor(colors.white)
  term.write((" "):rep(percNum))
  term.setCursorPos((w * 1) - (perc:len() - 1), y)
  term.setBackgroundColor(colors.black)
  term.setTextColor(term.isColor() and colors.yellow or colors.white)
  term.write(perc)
  term.setTextColor(colors.white)
  term.setBackgroundColor(colors.black)
  if newLine then
    print("")
  else
    term.setCursorPos(ox, oy)
  end
end
local function writeFile(path,content)
	local file = fs.open(path,"w")
	file.write(content)
	file.close()
end
local _, cy = term.getCursorPos()
function writeDown(input,dir)
  --print("Added \""..dir.."\" as a search directory and searching now...")
	for i,v in pairs(input) do
		if type(v) == "table" then
		  --print("Adding \""..dir.."/"..i.."\" as a search directory...")
			writeDown(v,dir.."/"..i)
		elseif type(v) == "string" then
			--print("Installing file \""..dir.."/"..i.."\"...")
      currentFileCount = currentFileCount + 1
			writeFile(dir.."/"..i,v)

			--print("Installed file \""..dir.."/"..i.."\".")
		end
	end
end

function getInput(txt)
	term.setTextColor(colors.green)
	write(txt)
	term.setTextColor(colors.lime)
	write("> ")
	term.setTextColor(colors.yellow)
	local input = io.read()
	term.setTextColor(colors.white)
	return input
end

print("Please input a destination folder:")
local dir = getInput(shell.getRunningProgram())
print()
writeDown(inputTable,dir)

print("Installed successfully.")

]]

local filesystem, fscancnt = explore(args[1])
local file = fs.open(args[2],"w")
file.write("inputTable = "..textutils.serialize(filesystem).."\ntotalFileCount = "..tostring(fscancnt).."\currentFileCount = 0\n\n\n\n\n\n"..append)
file.close()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 