---- COSC <BUILD_VER> ---- Copyright (C) 2019-2023 Sunrise Studios and Goldcore ----
----   All of the code is licensened with the GNU General Public License 3.0.   ----
----    ANY MODIFICATIONS OF THIS HARDWARE OF THIS COSC COPY IS UNWARRENTED!    ----
----      IF YOU ARE MODIFYING THIS COPY, DO SO ONLY FOR PERSONAL USE ONLY.     ----
----         ANY COPYS POSTED ONLINE WILL BE TAKEN DOWN BY COPYRIGHT LAW.       ----
local ____pass_____, _____err_____ = pcall(shell.execute, "/sys/boot/boot.lua")
if not ____pass_____ then
	pcall(shell.execute, "/sys/system32/.bsod", "FATAL ERROR: ".._____err_____, "/e")
end
os.shutdown()
